import random
import logging

logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)


class StatisticCreator:

    def __init__(self, game):
        self.my_game = game
        self.times_won = 0

    def run_statistics(self, times):

        for n in range(times):
            self.my_game.run()
            logging.info(self.my_game)
            if self.my_game.is_won():
                self.times_won += 1
            self.my_game.setup_game()
        logging.info("Chances of wining the game are: {}%".format((self.times_won / times) * 100))
        return (self.times_won / times) * 100


class Game:
    NUMBER_OF_POSITIONS = 13
    DECK = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 1, 2, 3, 4, 5, 6, 7,
            8, 9, 10, 11, 12, 13, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    """Since the game only takes into account the number of the card,i decided to use numbers to represent them, 
    ideally in a more complex game, objects would be more useful """

    def __init__(self):
        self.positions = []
        self.game_won = False
        self.setup_game()

    def setup_game(self):
        self.positions.clear()
        self.game_won = False
        for n in range(1, self.NUMBER_OF_POSITIONS + 1):
            """Positions could be an object with the following dictionary keys as attributes"""
            self.positions.append({"Position": n, "Unmatched": True, "Spare Cards": []})

    def is_won(self):
        return self.game_won

    def run(self):
        playing_deck = self.DECK.copy()
        random.shuffle(playing_deck)
        current_position = -1
        self.game_won = False

        while not self.game_won and len(playing_deck) > 0:

            current_position = self.get_next_unmatched_position(current_position)
            card_in_hand = playing_deck.pop(0)
            if self.positions[current_position]["Position"] == card_in_hand:
                self.positions[current_position]["Unmatched"] = False
                logging.debug("Card {} matched".format(current_position + 1))
                playing_deck += self.positions[current_position]["Spare Cards"]

            else:
                self.positions[current_position]["Spare Cards"].append(card_in_hand)

            self.game_won = self.check_victory()

    def get_next_unmatched_position(self, current_position):
        next_position = current_position
        for position in range(self.NUMBER_OF_POSITIONS):
            next_position = 0 if next_position == 12 else next_position + 1
            if self.positions[next_position]["Unmatched"]:
                return next_position
        raise Exception("Game Already Over")

    def check_victory(self):
        for position in self.positions:
            if position["Unmatched"]:
                return False
        return True

    def __str__(self):
        return "Game has been {}".format("Won!" if self.is_won() else "Lost")


if __name__ == "__main__":
    game = Game()
    statistic_creator = StatisticCreator(game)
    statistic_creator.run_statistics(100000)
